-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th5 14, 2019 lúc 02:58 PM
-- Phiên bản máy phục vụ: 10.1.37-MariaDB
-- Phiên bản PHP: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `yiidemo`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('admin', 1, 1555904399),
('admin', 9, NULL),
('admin', 11, 1556268883),
('manager-book', 2, 1555904399),
('manager-book', 9, NULL),
('manager-book', 11, 1556268883);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('admin', 1, NULL, NULL, NULL, 1555902640, 1555902640),
('create-book', 2, 'Create a book', NULL, NULL, 1555900351, 1555900351),
('delete-book', 2, 'Delete a book', NULL, NULL, 1555900573, 1555900573),
('index-book', 2, 'Index book', NULL, NULL, 1555900573, 1555900573),
('index-category', 2, 'Index Category', NULL, NULL, 1555902639, 1555902639),
('manager-book', 1, NULL, NULL, NULL, 1555900859, 1555900859),
('manager-category', 1, NULL, NULL, NULL, 1555903254, 1555903254),
('update-book', 2, 'Update a book', NULL, NULL, 1555900573, 1555900573),
('view-book', 2, 'View books', NULL, NULL, 1555900573, 1555900573);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('admin', 'manager-book'),
('admin', 'manager-category'),
('manager-book', 'delete-book'),
('manager-book', 'index-book'),
('manager-book', 'index-category'),
('manager-book', 'update-book'),
('manager-book', 'view-book'),
('manager-category', 'index-category');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `book`
--

CREATE TABLE `book` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cate` int(11) NOT NULL DEFAULT '0',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '0',
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL DEFAULT '0',
  `author` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `page` int(11) NOT NULL,
  `qty` int(11) NOT NULL DEFAULT '0',
  `publish_at` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `book`
--

INSERT INTO `book` (`id`, `name`, `cate`, `slug`, `status`, `image`, `desc`, `content`, `price`, `author`, `page`, `qty`, `publish_at`, `created_at`, `updated_at`) VALUES
(1, 'How to make yii project basic', 2, 'how-to-make-yii-project', 1, '', 'Sách chuyên về PHP(YII)', '@importing Bootstrap thông qua Less có thể yêu cầu việc điều chỉnh nơi lưu trữ font biểu tượng\r\nNếu như bạn @import mã nguồn Less của Bootstrap vào các tập tin Less của riêng bạn, bạn sẽ phải thay đổi biến số Less @icon-font-path để các đường dẫn tương đối trong url(...) có thể hoạt động được.', 100, 'Chó', 250, 12, 213131131, 1555319331, 1555333836),
(2, 'How to make yii project basic2', 2, 'how-to-make-yii-project2', 1, 'Screenshot (142).png', 'Sách chuyên về PHP(YII)2', 'ádadasd', 100123123, 'Chó', 2500, 120, 123131313, 1555320321, 1555320321),
(5, 'How to make yii project basic31231231313', 2, 'how-to-make-yii-project233322231', 1, 'http://localhost/Yii/backend/web/index.php/uploads/39557654_2343173282582832_613264906883432448_o.jpg', 'Sách chuyên về PHP(YII)', '<h2>sdad</h2>', 100, 'Chó', 250, 120, 13123131, 1556193357, 1556193357),
(8, 'How to make yii project basic231313', 2, 'how-to-make-yii-project12312332', 0, 'http://localhost/Yii/backend/web/index.php/uploads/Diagram.PNG', 'Sách chuyên về PHP(YII)', '<p style=\"text-align: left;\">21312</p>', 100, 'Chó', 250, 12, 13123131, 1556194248, 1556634291),
(10, 'How to make yii project basic1231313', 3, 'how-to-make-yii-project1232131231', 1, 'http://localhost/Yii/backend/web/uploads/41356499_501294520334850_5891225555512066048_n.jpg', 'Sách chuyên về PHP(YII)', '<p>2421423423</p>', 100, 'Chó', 250, 12, 13123131, 1556194377, 1556194377);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `status` smallint(6) NOT NULL DEFAULT '0',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `category`
--

INSERT INTO `category` (`id`, `name`, `slug`, `parent`, `status`, `created_at`, `updated_at`) VALUES
(2, 'PHP', 'PHP-Yii', 0, 1, 1555162154, 1555162253),
(3, 'PHP-Yii', 'php-mvc', 2, 0, 1555162164, 1555162164),
(4, 'Yii', 'yii', 0, 1, 1555162169, 1555162169),
(5, 'II', 'YII2', 3, 0, 1555310356, 1555310356),
(6, 'PHP-AC', 'php-ac', 5, 1, 1555310857, 1555310857);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `emails`
--

CREATE TABLE `emails` (
  `id` int(11) NOT NULL,
  `receiver_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `receiver_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `attachment` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `event`
--

CREATE TABLE `event` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `desc` text COLLATE utf8_unicode_ci NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `event`
--

INSERT INTO `event` (`id`, `title`, `desc`, `start`, `end`) VALUES
(1, 'Sinh nhat', 'Hôm nay sinh nhật của ai đó', '2019-05-18 00:00:00', '1969-12-31 00:00:00'),
(2, 'Thứ 2', 'Tuyệt vời\r\n', '2019-05-01 00:00:00', '1969-12-31 00:00:00'),
(3, 'Thứ 2', 'dấda', '2019-05-06 00:00:00', '1969-12-31 00:00:00'),
(4, 'Thứ 2', 'sadasd', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Thứ 2', 'ưdasdasd', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'tiec cuoi', 'djtffjgv', '2019-05-08 00:00:00', '1969-12-31 00:00:00'),
(7, 'Thứ 2', 'dadadad', '2019-05-09 00:00:00', '1969-12-31 00:00:00'),
(8, 'Thứ 2', 'dadadad', '2019-04-30 00:00:00', '1969-12-31 00:00:00'),
(9, 'tiec cuoi', 'ddd', '2019-05-04 00:00:00', '2019-05-04 00:00:00'),
(10, 'tiec cuoi', 'ddd', '2019-05-11 00:00:00', '1969-12-31 00:00:00'),
(11, 'Thứ 2', 'ccvv', '2019-05-03 00:00:00', '2019-05-03 00:00:00'),
(12, 'Thứ 2', 'ccvv', '2019-05-14 00:00:00', '1969-12-31 00:00:00'),
(13, 'Thứ 2', 'ddddddd', '2019-05-17 00:00:00', '1969-12-31 00:00:00'),
(14, 'Thứ 2', 'ddddddd', '2019-05-07 00:00:00', '1969-12-31 00:00:00'),
(15, 'Thứ 2', 'ddddddd', '2019-05-15 00:00:00', '1969-12-31 00:00:00'),
(16, 'tiec cuoi', 'ddddd', '2019-05-02 00:00:00', '1969-12-31 00:00:00'),
(17, 'tiec cuoi', 'ddddd', '2019-05-16 00:00:00', '1969-12-31 00:00:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `items`
--

CREATE TABLE `items` (
  `item_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_producer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_expiry_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `items`
--

INSERT INTO `items` (`item_name`, `item_producer`, `item_expiry_date`) VALUES
('Kem', 'Merino', '2019-04-26'),
('Kem 1', 'Vina-Milk', '2019-05-29'),
('Oreon', 'Vina-Milk', '0000-00-00'),
('Oreon 123', 'Vina-Milk', '2019-04-24'),
('Oreon dau', 'Vina-Milk', '2019-04-26'),
('Oreon i-cream 2', 'Vina-Milk', '0000-00-00'),
('Oreon-1', 'Vina-Milk', '2019-04-24'),
('Oreon2', 'Vina-Milk', '2019-04-05'),
('Oreon3', 'Vina-Milk', '2019-04-26'),
('Sữa', 'Vina-Milk', '2019-10-19'),
('Sữa Chua', 'Vina-Milk', '0000-00-00'),
('Sữa Dau', 'Vina-Milk', '2019-05-02'),
('Sữa Ông Thọ', 'Vina-Milk', '2019-05-03');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `leave`
--

CREATE TABLE `leave` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `desc` text COLLATE utf8_unicode_ci NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `leave`
--

INSERT INTO `leave` (`id`, `title`, `desc`, `start`, `end`) VALUES
(1, 'Sinh nhật', 'Sinh nhật của B\r\n', '2016-05-12 00:00:00', '1969-12-31 00:00:00'),
(2, 'Sinh nhật', 'Sinh nhật bạn B', '2019-05-14 00:00:00', '2019-05-14 00:00:00'),
(3, 'Tiệc cưới ', 'Tiệc cưới B', '2019-05-08 00:00:00', '2019-05-08 00:00:00'),
(4, 'Con ốm', 'Sốt', '2019-05-16 00:00:00', '1969-12-31 00:00:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1555156217),
('m130524_201442_init', 1555156221),
('m140506_102106_rbac_init', 1555898527),
('m170907_052038_rbac_add_index_on_auth_assignment_user_id', 1555898527),
('m180523_151638_rbac_updates_indexes_without_prefix', 1555898528),
('m190124_110200_add_verification_token_column_to_user_table', 1555156222),
('m190413_115159_category', 1555159078),
('m190415_082517_book', 1555317282),
('m190416_040917_my_orrder', 1555387944),
('m190416_041552_my_order', 1555388242),
('m190416_115834_my_order', 1555418390),
('m190416_124235_my_order', 1555418581),
('m190416_124459_product', 1555420405),
('m190416_131919_items', 1555420858),
('m190416_132422_items', 1555422643),
('m190416_135311_my_order', 1555423632);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `my_order`
--

CREATE TABLE `my_order` (
  `id` int(11) NOT NULL,
  `product_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(15,2) NOT NULL DEFAULT '0.00',
  `user_id` int(11) NOT NULL,
  `purchase_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `my_order`
--

INSERT INTO `my_order` (`id`, `product_name`, `price`, `user_id`, `purchase_date`) VALUES
(57, 'Oreon', '100000000.00', 12, '2019-05-04'),
(58, 'Kem', '100000000.00', 579, '2019-05-28'),
(60, 'Oreon', '100000000.00', 31231456, '2019-05-03'),
(61, 'Oreon2', '10.00', 31231456, '2019-05-04'),
(62, 'Sữa', '1.00', 31231456, '2019-05-11'),
(63, 'Oreon', '100000000.00', 31231456, '2019-05-29'),
(64, 'Oreon3', '100000000.00', 31231456, '2019-05-04'),
(65, 'Kem', '100000000.00', 234456, '2019-05-31'),
(66, 'Oreon', '100000000.00', 31231456, '2019-05-31'),
(71, 'Oreon', '100000000.00', 12456, '2019-05-29'),
(72, 'Kem', '100000000.00', 12456, '2019-06-07'),
(73, 'Oreon 123', '100000000.00', 31231456, '2019-05-11'),
(74, 'Kem', '100000000.00', 31231456, '2019-05-11'),
(75, 'Oreon', '100000000.00', 234456, '2019-04-05'),
(76, 'Kem', '100000000.00', 234456, '2019-06-05'),
(77, 'Oreon', '100000000.00', 234456, '2019-04-02'),
(78, 'Kem 1', '100000000.00', 31231456, '2019-04-03'),
(79, 'Kem 1', '100000000.00', 31231456, '2019-04-03'),
(80, 'Kem', '100000000.00', 31231456, '2019-05-03'),
(81, 'Kem', '100000000.00', 31231456, '2019-05-03'),
(82, 'Kem 1', '100000000.00', 31231456, '2019-05-28'),
(83, 'Kem 1', '100000000.00', 31231456, '2019-06-03'),
(84, 'Oreon', '10000.00', 31231456, '2019-06-05'),
(85, 'Kem 1', '10000.00', 31231456, '2019-05-03'),
(86, 'Kem', '100000000.00', 31231456, '2019-05-04'),
(87, 'Kem', '100000000.00', 31231456, '2019-05-03'),
(88, 'Kem 1', '100000000.00', 31231456, '2019-05-04'),
(89, 'Oreon 123', '100000000.00', 31231456, '2019-05-02');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `verification_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `verification_token`) VALUES
(1, 'admin', 'E4_Ia1he5--KV1ADR_vmStSmW3Ofjtsd', '$2y$13$PWfbgyLgclmJ4Up1x/Wrtu5c/UfE4liY.V1KB74eknZZawaOkJNYG', NULL, 'abc@gmail.com', 0, 1555156274, 1555156274, 'caT5o6Snrvvx_CKk85lRybSwv8pByBvY_1555156274'),
(2, 'bookmanager', 'uvgnihdJ1CltOf_CCQJSa8cinJa52264', '$2y$13$YZpVnZ9JwmA/IqcCSFL8hubVD2ujhya.UKYUeyoc9zEKFz9yRIUPG', NULL, 'bookmanager@gmail.com', 9, 1555904153, 1555904153, 'eaLgQ2MIc3SZIzfD1XVCi4wxnKkMdgp9_1555904153'),
(9, 'quanh', '8YbxTquHIQZ_7hEJMJDuXQSajZC8WO71', '$2y$13$cfruCZKcQCwLY8S7z3vdS.bD3RNLykMFxnaJCB/DgJNnMbuQ1wcaq', NULL, 'quang@gmail.com', 9, 1556268727, 1556268727, 'vF1AePIDgPQNWTw-pI0QS4FXAOq7VX7I_1556268727'),
(11, 'admin-21', 'sCT31G4AIqltT5AZAJwcZYFGls7It3lC', '$2y$13$GmcB3G0kNk.Ckb5/L/MCweP.sLzUnMMBXEcL.VSkqogYcpepncH2S', NULL, 'sam@gmail.com', 9, 1556268883, 1556268883, 'Y7NXMey-AfuN4Zc8-XqREyQ4VOvuOzNL_1556268883');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`),
  ADD KEY `idx-auth_assignment-user_id` (`user_id`);

--
-- Chỉ mục cho bảng `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Chỉ mục cho bảng `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Chỉ mục cho bảng `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Chỉ mục cho bảng `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Chỉ mục cho bảng `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Chỉ mục cho bảng `emails`
--
ALTER TABLE `emails`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`item_name`);

--
-- Chỉ mục cho bảng `leave`
--
ALTER TABLE `leave`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Chỉ mục cho bảng `my_order`
--
ALTER TABLE `my_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-my_order-product_name` (`product_name`);

--
-- Chỉ mục cho bảng `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `book`
--
ALTER TABLE `book`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT cho bảng `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `emails`
--
ALTER TABLE `emails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `event`
--
ALTER TABLE `event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT cho bảng `leave`
--
ALTER TABLE `leave`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `my_order`
--
ALTER TABLE `my_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT cho bảng `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `my_order`
--
ALTER TABLE `my_order`
  ADD CONSTRAINT `fk-post-product_name` FOREIGN KEY (`product_name`) REFERENCES `items` (`item_name`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
