<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\my_order\models\ItemsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Items');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="items-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <!-- Cần giá trị đường dẫn @value=>Url::to('index.php?=my_order/items/create') -->
        <?= Html::button(Yii::t('app', 'Create Items'), ['value'=>Url::to('http://localhost/Yii/backend/web/index.php/my_order/items/create'),'class' => 'btn btn-success','id'=>'modalButton' ]) ?>
    </p>

    <?php 
       Modal::begin([
            'header' => '<h4>Items</h4>', // Tiêu đề
            'id'     => 'modal', 
            'size'   => 'modal-xs',
       ]);
       echo '<div id="modalContent"></div>'; // Hiển thị thông tin của bảng mình cần hiện lên
       Modal::end();
    ?>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php Pjax::begin(['id'=>'itemsGrid']); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn',
             'header' => 'STT',
             'headerOptions'  => ['style' => 'padding:20px'],
             'contentOptions' => ['style' => 'text-align:center']
            ],

            [
                'attribute' => 'item_name',
                'contentOptions' => ['style' => 'font-size:larger;font-weight: bold; text-align:right']
            ],
            'item_producer',
            [
                'attribute' => 'item_expiry_date',
                'value'     => 'item_expiry_date',
                'format'    =>'raw',
                'filter'    => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'item_expiry_date',
                      'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                    ]
                ]),
            ],

            ['class' => 'yii\grid\ActionColumn',
             'header' => 'Thao tác',
             'headerOptions' => ['style' => 'whith:15px; text-align:center' ],
             'contentOptions' => ['style' => 'whith:15px; text-align:center' ],
             'buttons' => [
                'view' => function ($url, $model)
                {
                    return Html::a('View',$url,['class' => 'btn btn-xs btn-info']);
                },
                'update' => function($url, $model){
                    return Html::a('Update',$url,['class' => 'btn btn-xs btn-warning']);
                },
                'delete' => function($url, $model){
                    
                }
             ]
            ],
        ],
    ]); ?>
  
    <?php Pjax::end(); ?>

</div>
