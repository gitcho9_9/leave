<?php

namespace app\modules\my_order\models;

use Yii;

/**
 * This is the model class for table "items".
 *
 * @property string $item_name
 * @property string $item_producer
 * @property string $item_expiry_date
 *
 * @property MyOrder[] $myOrders
 */
class Items extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'items';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item_name', 'item_producer', 'item_expiry_date'], 'required','message'=>'{attrubute} không được để trống'],
            [['item_expiry_date'], 'safe'],
            [['item_name', 'item_producer'], 'string', 'max' => 255],
            [['item_name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'item_name' => Yii::t('app', 'Sản phẩm'),
            'item_producer' => Yii::t('app', 'Nhà sản xuất'),
            'item_expiry_date' => Yii::t('app', 'Ngày hết hạn sản phẩm'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMyOrders()
    {
        return $this->hasMany(MyOrder::className(), ['product_name' => 'item_name']);
    }
}
