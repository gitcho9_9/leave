<?php

namespace app\modules\my_order\models;

use Yii;


/**
 * This is the model class for table "my_order".
 *
 * @property int $id
 * @property string $product_name
 * @property string $price
 * @property int $user_id
 * @property string $purchase_date
 *
 * @property Items $productName
 */
class MyOrder extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'my_order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_name', 'user_id', 'purchase_date'], 'required'],
            [['price'], 'number'],
            [['user_id'], 'integer'],
            [['purchase_date'], 'safe'],
            [['product_name'], 'string', 'max' => 255],
            [['product_name'], 'exist', 'skipOnError' => true, 'targetClass' => Items::className(), 'targetAttribute' => ['product_name' => 'item_name']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert){
        if (parent::beforeSave($insert)) {
            $maso = '456';
            $Vnd = 'VND';
            $this->price *= 10;
            $this->price = $this->price;
            $this->user_id = $this->user_id.$maso; 
            return true;
        }
        return false;
    }
    public function afterSave($insert,$changedAttributes){
         parent::afterSave($insert, $changedAttributes);
         if($insert) {
            $historyModel = new MyOrder();
            if(isset($changedAttributes['price']  )){
                $historyModel->price = $changedAttributes['price'];
            }     
            $historyModel->save(true);
        }
    }
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'product_name' => Yii::t('app', 'Tên sản phẩm'),
            'price' => Yii::t('app', 'Giá'),
            'user_id' => Yii::t('app', 'User ID'),
            'purchase_date' => Yii::t('app', 'Ngày mua'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductName()
    {
        return $this->hasOne(Items::className(), ['item_name' => 'product_name']);
    }
}
