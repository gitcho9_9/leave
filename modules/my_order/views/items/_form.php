<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\widegts\Pjax;
use backend\modules\my_order\models\Items;
use dosamigos\datepicker\DatePicker;


/* @var $this yii\web\View */
/* @var $model app\modules\my_order\models\Items */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="items-form">

    <?php $form = ActiveForm::begin(['id'=>$model->formName()]); ?>

    <?= $form->field($model, 'item_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'item_producer')->textInput(['maxlength' => true]) ?>

    <!-- <?= $form->field($model, 'item_expiry_date')->textInput() ?> -->
     <?= $form->field($model, 'item_expiry_date')->widget(
    DatePicker::className(), [
        // inline too, not bad
         'inline' => false, 
         // modify template for custom rendering
        // 'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
    ]);?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php 
$script = <<< JS

$('form#{$model->formName()}').on('beforeSubmit', function(e)
{
  var \$form = $(this);
   $.post(
       \$form.attr("action"),
       \$form.serialize()
   )
   .done(function(result){
   	 if(result == 1){
   	 	$(document).find('#modal').modal('hide');
   	 	$(\$form).trigger("reset");
   	 	$.pjax.reload({container:'#itemsGrid'});
   	 }else{
   	 	$("#message").html(result);
   	 }
   }).fail(function(){
     console.log('server error');
   	}); 
   	return false;
 });
JS;
$this->registerJs($script);
?> 
