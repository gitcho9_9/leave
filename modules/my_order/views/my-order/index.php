<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use backend\moduls\my_order\models\MyOrder;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'My Orders');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="my-order-index">

    <h1><?= Html::encode($this->title) ?></h1>
  <!-- 
     <p>
        <?= Html::button(Yii::t('app', 'Create My Order'), ['value'=>Url::to('index.php/my_order/my-order/create'),'class' => 'btn btn-success','id'=>'modalButton']) ?>
    </p> --> 
     <?php 
       Modal::begin([
            'header' => '<h4>My Order</h4>', 
            'id'     => 'modal', 
            'size'   => 'modal-xs',
       ]);
       echo '<div id="modalContent"></div>'; // Hiển thị thông tin của bảng mình cần hiện lên
       Modal::end();
    ?>
     
      <?= $this->render('_form', [
        'model' => $model,
    ]) ?>   

    <?php Pjax::begin(['id' => 'myorderGrid']); ?>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\checkBoxColumn'],

            [
                'attribute' => 'id',
                'headerOptions' => ['style' => 'whith:15px; text-align:center'],
                'contentOptions' => ['style' => 'whith:15px; text-align:center']
            ],
            
            [
                'attribute' => 'product_name',
                'headerOptions' => ['style' => 'whith:15px; text-align:center'],
                'contentOptions' => ['style' => 'whith:15px; text-align:center']
            ],
            
             [
                'attribute' => 'price',
                'headerOptions' => ['style' => 'whith:15px; text-align:center'],
                'contentOptions' => ['style' => 'whith:15px; text-align:center']
            ],
           
            [
                'attribute' =>  'user_id',
                'headerOptions' => ['style' => 'whith:15px; text-align:center'],
                'contentOptions' => ['style' => 'whith:15px; text-align:center']
            ],
            
            [
                'attribute' => 'purchase_date',
                'headerOptions' => ['style' => 'whith:15px; text-align:center'],
                'content' => function($model){
                      return date($model->purchase_date);
 
                }
            ],


            ['class' => 'yii\grid\ActionColumn',
             'header' => 'Thao tác',
             'headerOptions' => ['style'=>'whith:15px, text-align:center'],
             'buttons' => [
                'view' => function ($url, $model)
                {
                    return Html::a('View',$url,['class' => 'btn btn-xs btn-info']);
                },
                'update' => function ($url, $model){
                    return Html::a('Eidit',$url,['class' => 'btn btn-xs btn-warning']);
                },
                'delete' => function ($url,$model) {
                    return Html::a(Yii::t('yii', 'Delete'), '#', [
                        'title' => Yii::t('yii', 'Delete'),
                        'aria-label' => Yii::t('yii', 'Delete'),
                        'class' => "btn btn-xs btn-danger",
                        'onclick' => "
                        if (confirm('You want to delete')) {
                            $.ajax('$url', {
                                type: 'POST'
                                }).done(function(data) {
                                    $.pjax.reload({container: '#myorderGrid'});
                                    });
                                }
                                return false;
                                ",
                            ]);
                },
             ]
            ],
        ],
    ]); ?>
</div>
<?php Pjax::end(); ?>

<!-- <//?php
 $this->registerJs("
     $(document).on('ready pjax:success', function() {
         $('.pjax-delete-link').on('click', function(e) {
             e.preventDefault();
             var deleteUrl = $(this).attr('delete-url');
             var pjaxContainer = $(this).attr('pjax-container');
             var result = confirm('Delete this item, are you sure?');                                
             if(result) {
                 $.ajax({
                     url: deleteUrl,
                     type: 'post',
                     error: function(xhr, status, error) {
                         alert('There was an error with your request.' + xhr.responseText);
                     }
                 }).done(function(data) {
                     $.pjax.reload('#' + $.trim(pjaxContainer), {timeout: 3000});
                 });
             }
         });

     });
 ");
?>
 -->