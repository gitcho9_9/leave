<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\widegts\Pjax;
use app\modules\my_order\models\MyOrder;
use app\modules\my_order\models\Items;
use dosamigos\datepicker\DatePicker; // Sử dụng hàm date

$cat = new MyOrder;

/* @var $this yii\web\View */
/* @var $model app\modules\my_order\models\MyOrder */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="my-order-form">
    <?php yii\widgets\Pjax::begin(['id' => 'my_order']) ?>
    <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true ]]); ?>

    <?= $form->field($model, 'product_name')->dropDownList(
      ArrayHelper::map(Items::find()->all(),'item_name','item_name'),
      [ 
        'prompt' => 'Lựa chọn danh mục'
      ]
    ); ?>

    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

   <!--  <?= $form->field($model, 'purchase_date')->textInput() ?> -->
    <?= $form->field($model, 'purchase_date')->widget(
		DatePicker::className(), [
		    // inline too, not bad
		     'inline' => false, 
		     // modify template for custom rendering
		    // 'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
		    'clientOptions' => [
		        'autoclose' => true,
		        'format' => 'yyyy-mm-dd'
		    ]
    ]);?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <?php yii\widgets\Pjax::end() ?>
</div>


<?php 
  $this->registerJs('
    $("document").ready(function(){
      $("#my_order").on("pjax:end", function(){
        $.pjax.reload({container:"#myorderGrid"})
        });
      });
    ');
?>

<!-- <?php 
 $script = <<< JS

 $('form#{$model->formName()}').on('beforeSubmit', function(e)
 {
   var \$form = $(this);
    $.post(
        \$form.attr("action"),
        \$form.serialize()
    )
    .done(function(result){
   	 if(result == 1){
   	 	$(document).find('#modal').modal('hide');
    	 	$(\$form).trigger("reset");
  	 	$.pjax.reload({container:'#myorderGrid'});
   	 }else{
   	 	$("#message").html(result);
    	 }
   }).fail(function(){
      console.log('server error');
    	}); 
   	return false;
  });
 JS;
 $this->registerJs($script);
?> 
 -->