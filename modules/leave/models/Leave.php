<?php

namespace app\modules\leave\models;

use Yii;

/**
 * This is the model class for table "{{%leave}}".
 *
 * @property int $id
 * @property string $title
 * @property string $desc
 * @property string $start
 * @property string $end
 */
class Leave extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%leave}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'desc', 'start', 'end'], 'required'],
            [['desc'], 'string'],
            [['start', 'end'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'desc' => Yii::t('app', 'Desc'),
            'start' => Yii::t('app', 'Start'),
            'end' => Yii::t('app', 'End'),
        ];
    }
}
