<?php

namespace app\modules\leave\controllers;

use Yii;
use app\modules\leave\models\Leave;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\base\ViewNotFoundException;

/**
 * LeaveController implements the CRUD actions for Leave model.
 */
class LeaveController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Leave models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Leave::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionForm($start,$end)
        {
             $model = new Leave();
             $model->start = $start;
             $model->end  = $end;
            return $this->renderAjax('_form',[
                'model'=>$model,       
            ]);
        }

        public function actionDropChild($id,$start,$end){
            echo "ID=".$id." START=".$start." EBD=".$end;
            $model = Leave::findOne(['id'=>$id]);

            $model->start = $start;
            $model->end = $end;

           $model->save();
        }

        public function actionEventCalendarSchedule()
        {
            $aryEvent = Leave::find()->asArray()->all();

            return Json::encode($aryEvent);
        }

        public function actionResourceCalendarSchedule()
        {
            $aryResource=[
                    ['id' => 'a', 'title' => 'Daily Report'],
                    ['id' => 'b', 'title' => 'Auditorium B', 'eventColor' => 'green'],
                    ['id' => 'c', 'title' => 'Auditorium C', 'eventColor' => 'orange'],
                    [
                        'id'       => 'd', 'title' => 'Auditorium D',
                        'children' => [
                            ['id' => 'd1', 'title' => 'Room D1'],
                            ['id' => 'd2', 'title' => 'Room D2'],
                        ],
                    ],
                    ['id' => 'e', 'title' => 'Auditorium E'],
                    ['id' => 'f', 'title' => 'Auditorium F', 'eventColor' => 'red'],
                    ['id' => 'g', 'title' => 'Auditorium G'],
                    ['id' => 'h', 'title' => 'Auditorium H'],
                    ['id' => 'i', 'title' => 'Auditorium I'],
                    ['id' => 'j', 'title' => 'Auditorium J'],
                    ['id' => 'k', 'title' => 'Auditorium K'],
                    ['id' => 'l', 'title' => 'Auditorium L'],
                    ['id' => 'm', 'title' => 'Auditorium M'],
                    ['id' => 'n', 'title' => 'Auditorium N'],
                    ['id' => 'o', 'title' => 'Auditorium O'],
                    ['id' => 'p', 'title' => 'Auditorium P'],
                    ['id' => 'q', 'title' => 'Auditorium Q'],
                    ['id' => 'r', 'title' => 'Auditorium R'],
                    ['id' => 's', 'title' => 'Auditorium S'],
                    ['id' => 't', 'title' => 'Auditorium T'],
                    ['id' => 'u', 'title' => 'Auditorium U'],
                    ['id' => 'v', 'title' => 'Auditorium V'],
                    ['id' => 'w', 'title' => 'Auditorium W'],
                    ['id' => 'x', 'title' => 'Auditorium X'],
                    ['id' => 'y', 'title' => 'Auditorium Y'],
                    ['id' => 'z', 'title' => 'Auditorium Z'],
                ];

            return Json::encode($aryResource);
        }

    /**
     * Displays a single Leave model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Leave model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($start,$end)
    {
        $model = new Leave();
        $model->start = $start;
        $model->end  = $end;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Leave model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Leave model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Leave model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Leave the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Leave::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
