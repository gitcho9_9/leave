<?php
$this->title = 'Quản lý ảnh';

$baseUrl = str_replace('/Yii/backend/web','', Yii::$app->urlManager->baseUrl);
?>

<div class="file-index">
	<div class="panel panel-primary">
	 <div class="panel-heading"><h3 class="panel-title"><?= $this->title; ?></h3></div>
	  <div class="panel-body">
	    <iframe src="<?= $baseUrl ?>/file/dialog.php" style="width: 100%;height: 500px;border: none;"></iframe>	
	  </div>
    </div>
</div>
