<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use ptrnov\fullcalendar\FullcalendarScheduler;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\web\JsExpression;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Leaves');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="leave-index">

    <?php Pjax::begin(); ?>
     
    <?php  
     $JSEventClick = <<<EOF
            function(calEvent, jsEvent, view) {
                alert('test');
            }
    EOF;
    $wgCalendar=FullcalendarScheduler::widget([     
            'modalSelect'=>[
                /**
                 * modalSelect for cell Select
                 * 'clientOptions' => ['selectable' => true]                    //makseure set true.
                 * 'clientOptions' => ['select' => function or JsExpression]    //makseure disable/empty. if set it, used JsExpressio to callback.          
                 * @author piter novian [ptr.nov@gmail.com]                     //"https://github.com/ptrnov/yii2-fullcalendar".
                */
                'id' => 'modal-select',                                         //set it, if used FullcalendarScheduler more the one on page.
                'id_content'=>'modalContent',                                   //set it, if used FullcalendarScheduler more the one on page.
                'headerLabel' => 'Model Header Label',                          //your modal title,as your set. 
                'modal-size'=>'modal-lg'                                        //size of modal (modal-xs,modal-sm,modal-sm,modal-lg).
            ],
            'header'        => [
                'left'   => 'today prev,next',
                'center' => 'title',
                'right'  => 'timelineOneDays,agendaWeek,month,listWeek',
            ],
            'options'=>[
                'id'=> 'calendar_test',                                         //set it, if used FullcalendarScheduler more the one on page.
                'language'=>'id',
            ],
            'optionsEventAdd'=>[
                'events' => Url::to(['/leave/leave/event-calendar-schedule']),         //should be set "your Controller link"  
                'resources'=> Url::to(['/leave/leave/resource-calendar-schedule']),        //should be set "your Controller link" 
                //disable 'eventDrop' => new JsExpression($JSDropEvent),
                'eventDropUrl'=>'http://localhost/Yii/leave/leave/drop-child',                             //should be set "your Controller link" to get(start,end) from select. You can use model for scenario.
                'eventSelectUrl'=>'http://localhost/Yii/leave/leave/create',                             //should be set "your Controller link" to get(start,end) from select. You can use model for scenario            
            ],              
            'clientOptions' => [
                'language'=>'id',
                'selectable' => true,
                'selectHelper' => true,
                'droppable' => true,
                'editable' => true,
                //'select' => new JsExpression($JSCode),                        // don't set if used "modalSelect"
                'eventClick' => new JsExpression($JSEventClick),
                
                'firstDay' =>'0',
                'theme'=> true,
                'aspectRatio'=> 1.8,
                //'scrollTime'=> '00:00', // undo default 6am scrollTime
                'defaultView'=> 'timelineDay',//'timelineDay',//agendaDay',
                'views'=> [
                    'timelineOneDays' => [
                        'type'     => 'timeline',
                        'duration' => [
                            'days' => 1,
                        ],
                    ], 

                ],              
                'resourceLabelText' => 'Discriptions',
                'resourceColumns'=>[
                        [
                            'labelText'=> 'Parent',
                            'field'=> 'title'
                        ],
                        [
                            'labelText'=> 'Subject',
                            'field'=> 'title'
                        ],
                        [
                            'labelText'=> 'Occupancy',
                            'field'=> 'create_at'
                        ]
                ],
                'resources'=> Url::to(['/leave/leave/resource-calendar-schedule']),        //should be set "your Controller link" 
                'events' => Url::to(['/leave/leave/event-calendar-schedule']),             //should be set "your Controller link"  
            ],  

        ]); 
    ?>
    <?=$wgCalendar?>

    <?php Pjax::end(); ?>

</div>
