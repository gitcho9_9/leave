<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widegts\Pjax;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\modules\leave\models\Leave */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="leave-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'desc')->textarea(['rows' => 6]) ?>

    <!-- <//? = $form->field($model, 'start')->textInput() ?>  -->

    <?= $form->field($model, 'start')->widget(
		DatePicker::className(), [
		    // inline too, not bad
		     'inline' => false, 
		     // modify template for custom rendering
		    // 'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
		    'clientOptions' => [
		        'autoclose' => true,
		        'format' => 'yyyy-mm-dd'
		    ]
    ]);?>


     <?= $form->field($model, 'end')->widget(
		DatePicker::className(), [
		    // inline too, not bad
		     'inline' => false, 
		     // modify template for custom rendering
		    // 'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
		    'clientOptions' => [
		        'autoclose' => true,
		        'format' => 'yyyy-mm-dd'
		    ]
    ]);?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
