<?php

namespace app\modules\my_order\controllers;

use Yii;
use app\modules\my_order\models\MyOrder;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MyOrderController implements the CRUD actions for MyOrder model.
 */
class MyOrderController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MyOrder models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new MyOrder();
        
        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            $model = new MyOrder(); //reset model
        }

        $dataProvider = new ActiveDataProvider([
            'query' => MyOrder::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    /**
     * Displays a single MyOrder model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MyOrder model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MyOrder();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()){
                echo 1;
            }else {
                echo 0;
            }
            //return $this->redirect(['view', 'id' => $model->id]);
        }else{
            return $this->renderAjax('create', [
            'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing MyOrder model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing MyOrder model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
      public function actionDelete($id)
     {
         $this->findModel($id)->delete();
        if (!Yii::$app->request->isAjax) {
            return $this->redirect(['index']);
        }
     }

    /**
     * Finds the MyOrder model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MyOrder the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MyOrder::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
